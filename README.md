The app gets data from a JSON file and displays the products in a grid with options to add or remove items to the cart and shows the cart summary too.
###  After cloning the repo, follow the steps:
```sh
$ npm install
$ bower install
node app
```

Access the app at http://localhost:3000