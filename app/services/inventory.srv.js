'use strict';
cm.service('inventory', function($http, $q, $rootScope) {
    this.itemsInCart = {};
    var self = this;
    this.getData = function() {
            var deferred = $q.defer();
            $http.get("app/data/inventory.json").success(function(response) {
                self.itemsInCart = localStorage['itemsInCart'] ? JSON.parse(localStorage['itemsInCart']) : {};
                for(var key in self.itemsInCart) {
                    response.products.forEach(function(product) {
                        if(product.id === key) {
                            product.count = self.itemsInCart[key].count;
                            product.total = self.itemsInCart[key].total;
                        }
                    });
                }
                deferred.resolve({data : response.products});
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
    };
    this.getItemsInCart = function() {
        this.itemsInCart = localStorage['itemsInCart'] ? JSON.parse(localStorage['itemsInCart']) : {};
        return this.itemsInCart;
    };
    this.addItemsToCart = function(item) {
        this.itemsInCart[item.id] = this.itemsInCart[item.id] || {};
        this.itemsInCart[item.id]['total'] = this.itemsInCart[item.id]['total'] ? this.itemsInCart[item.id]['total'] + (+item.price) : item.price;
        this.itemsInCart[item.id] = item;
        if(!this.itemsInCart[item.id].count) {
            this.itemsInCart[item.id].count = 1;
        } else {
            this.itemsInCart[item.id].count = ++this.itemsInCart[item.id].count;
        }
        $rootScope.$emit('CART_MODIFIED', this.itemsInCart);
        localStorage['itemsInCart'] = JSON.stringify(this.itemsInCart);
        return this.itemsInCart[item.id];
    };
    this.removeItemsFromCart = function(item, removeAll) {
        this.itemsInCart[item.id] = this.itemsInCart[item.id] || {};
        this.itemsInCart[item.id]['total'] = this.itemsInCart[item.id] && this.itemsInCart[item.id]['total'] ? this.itemsInCart[item.id]['total'] + (+item.price) : this.itemsInCart[item.id]['total'];
        this.totalCount++;
        if(!removeAll && this.itemsInCart[item.id] && this.itemsInCart[item.id].count > 0) {
            this.itemsInCart[item.id].count = --this.itemsInCart[item.id].count;
            if(this.itemsInCart[item.id].count === 0) {
                delete this.itemsInCart[item.id];
            }
        } else {
            delete this.itemsInCart[item.id];
        }
        $rootScope.$emit('CART_MODIFIED', this.itemsInCart);
        localStorage['itemsInCart'] = JSON.stringify(this.itemsInCart);
        return this.itemsInCart[item.id];
    };
});