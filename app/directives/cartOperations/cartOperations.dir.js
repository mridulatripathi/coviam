'use strict';
cm.directive('cartOperations', function() {
    return {
        restrict: 'E',
        templateUrl: "app/directives/cartOperations/cartOperations.html",
        controller: function($scope, inventory, $rootScope) {
            $scope.totalQuantity = $scope.product;
            $scope.addToCart = function() {
                $scope.totalQuantity = inventory.addItemsToCart($scope.product);
            };
            $scope.removeFromCart = function() {
                $scope.totalQuantity = inventory.removeItemsFromCart($scope.product);
            };
            $rootScope.$on('CART_MODIFIED', function (e, itemsInCart) {
                $scope.totalQuantity = itemsInCart[$scope.product['id']];
                $scope.product.count = $scope.totalQuantity ? $scope.totalQuantity.count : 0;
            });

        }
    }
});