'use strict';
cm.directive('cartGrid', function() {
    return {
        restrict: 'E',
        templateUrl: "app/directives/cartGrid/cartGrid.html",
        controller: function($scope, inventory) {
            $scope.products  = [];
            inventory.getData().then(function(products) {
                $scope.products = products.data;
            });
        }
    }
});