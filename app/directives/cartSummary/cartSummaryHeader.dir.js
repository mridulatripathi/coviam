'use strict';
cm.directive('cartSummaryHeader', function () {
    return {
        restrict: 'E',
        templateUrl: "app/directives/cartSummary/cartSummaryHeader.html",
        controller: function ($scope, inventory, $rootScope) {
            function getParsedInfo(itemsInCart) {
                $scope.totalCount = 0;
                $scope.totalAmount = 0;
                $scope.itemsInCart = itemsInCart;
                for (var key in itemsInCart) {
                    $scope.totalCount += itemsInCart[key].count;
                    $scope.totalAmount += itemsInCart[key].count * (+itemsInCart[key].price);
                }
            }

            $scope.itemsInCart = inventory.getItemsInCart();
            getParsedInfo($scope.itemsInCart);
            $rootScope.$on('CART_MODIFIED', function (e, itemsInCart) {
                getParsedInfo(itemsInCart);
            });
        }
    }
});